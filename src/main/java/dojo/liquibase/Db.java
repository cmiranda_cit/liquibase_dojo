package dojo.liquibase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public final class Db {

	private static Connection conn;

	static {

		try {

			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:test.db");
			conn.setAutoCommit(true);

		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

		System.out.println("Opened database successfully");
	}

	public static boolean executeUpdate(final String sql) {

		System.out.println("sql=" + sql);

		Statement stmt = null;
		try {

			try {

				stmt = conn.createStatement();
				stmt.executeUpdate(sql);
				
				return true;

			} finally {
				stmt.close();
			}

		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Ops! " + e.getMessage());
			return false;
		}

	}

	public static List find(final String query, RowMapper rm) {

		System.out.println("query=" + query);

		List result = new ArrayList();
		Statement stmt = null;
		ResultSet rs = null;
		try {

			try {

				stmt = conn.createStatement();
				rs = stmt.executeQuery(query);

				while (rs.next()) {
					result.add(rm.map(rs));
				}

			} finally {
				stmt.close();
				rs.close();
			}

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Ops! " + e.getMessage());
		}

		return result;

	}

	public static Object findFirst(final String query, RowMapper rm) {
		List result = find(query, rm);
		return result.isEmpty() ? null : result.get(0);
	}

	public static Connection getConnection() {
		return conn;
	}

}
