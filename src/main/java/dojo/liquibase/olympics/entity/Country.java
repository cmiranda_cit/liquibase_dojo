package dojo.liquibase.olympics.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import dojo.liquibase.DbEntity;
import dojo.liquibase.RowMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Country extends DbEntity<Country> {

	private String acronym;

	@Override
	protected String tableName() {
		return "country";
	}

	@Override
	protected String insertSqlCommand() {
		return String.format("insert into %s (name, acronym) values ('%s', '%s')", this.tableName(), this.getName(), this.getAcronym());
	}

	@Override
	protected String updateSetSqlCommand() {
		return String.format("acronym = '%s'", this.getAcronym());
	}

	@Override
	protected RowMapper<Country> rowMapper() {
		return new RowMapper<Country>() {
			public Country map(ResultSet rs) {
				Country result = new Country();

				try {

					result.setId(rs.getLong("id"));
					result.setName(rs.getString("name"));

				} catch (SQLException e) {
					System.err.println("Fail! " + e.getMessage());
				}

				return result;
			}
		};
	}

}
