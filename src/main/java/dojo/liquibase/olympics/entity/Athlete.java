package dojo.liquibase.olympics.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import dojo.liquibase.DbEntity;
import dojo.liquibase.RowMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Athlete extends DbEntity<Athlete> {

	private Sport sport;
	private Country country;

	protected String tableName() {
		return "athlete";
	}

	protected String insertSqlCommand() {
		//
		return String.format("insert into %s (name, sportId, countryId) values ('%s', %d, %d)", this.tableName(),
				this.getName(), this.getSport().getId(), this.getCountry().getId());
	}

	protected String updateSetSqlCommand() {
		return String.format("name = '%s', sportId = %d, countryId = %d", this.getName(), this.getSport().getId(),
				this.getCountry().getId());
	}

	protected RowMapper<Athlete> rowMapper() {
		return new RowMapper<Athlete>() {
			public Athlete map(ResultSet rs) {

				Athlete result = new Athlete();
				try {

					result.setId(rs.getLong("id"));
					result.setName(rs.getString("name"));
					result.setCountry(new Country().findById(rs.getLong("countryId")));
					result.setSport(new Sport().findById(rs.getLong("sportId")));

				} catch (SQLException e) {
					System.err.println("Fail! " + e.getMessage());
				}

				return result;
			}
		};
	}

}
