package dojo.liquibase.olympics.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import dojo.liquibase.DbEntity;
import dojo.liquibase.RowMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Sport extends DbEntity<Sport> {

	private String description;

	@Override
	protected String tableName() {
		return "sport";
	}

	@Override
	protected String insertSqlCommand() {
		return String.format("insert into %s (name, description) values ('%s', '%s')", this.tableName(), this.getName(),
				this.getDescription());
	}

	@Override
	protected String updateSetSqlCommand() {
		return String.format("description = '%s'", this.getDescription());
	}

	@Override
	protected RowMapper<Sport> rowMapper() {
		return new RowMapper<Sport>() {

			public Sport map(ResultSet rs) {
				Sport result = new Sport();

				try {

					result.setId(rs.getLong("id"));
					result.setName(rs.getString("name"));
					result.setDescription(rs.getString("description"));

				} catch (SQLException e) {
					System.err.println("Fail! " + e.getMessage());
				}

				return result;
			}
		};
	}

}
