package dojo.liquibase;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public abstract class DbEntity<T> {

	@Getter
	@Setter
	private long id;

	@Getter
	@Setter
	private String name;

	public boolean save() {
		return Db.executeUpdate(insertSqlCommand());
	}

	public void delete() {
		Db.executeUpdate("delete from " + tableName() + " where id = " + this.id);
	}

	public void update() {
		Db.executeUpdate("update " + tableName() + " set name = " + this.name + ", " + updateSetSqlCommand()
				+ " where id = " + this.id);
	}

	public List findAll() {
		return Db.find("select * from " + tableName(), rowMapper());
	}

	public T findByName(String name) {
		return (T) Db.findFirst(String.format("select * from %s where name = '%s'", tableName(), name), rowMapper());
	}

	public T findById(long id) {
		return (T) Db.findFirst(String.format("select * from %s where id = %d", tableName(), id), rowMapper());
	}

	protected abstract String tableName();

	protected abstract String insertSqlCommand();

	protected abstract String updateSetSqlCommand();

	protected abstract RowMapper<T> rowMapper();

}
