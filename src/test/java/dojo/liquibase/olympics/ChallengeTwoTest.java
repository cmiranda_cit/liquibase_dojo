package dojo.liquibase.olympics;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import dojo.liquibase.AbstractLiquibaseBootstrapTest;
import dojo.liquibase.olympics.entity.Athlete;

public class ChallengeTwoTest extends AbstractLiquibaseBootstrapTest {

    @Before
    public void setup() {

    }

    @Test
    public void shouldInsertRafaelaSilvaAthlete() {

        Athlete marta = new Athlete().findByName("Marta");

        Assert.assertNotNull(marta);
        Assert.assertEquals("Marta", marta.getName());
        Assert.assertEquals("Brasil", marta.getCountry().getName());
        Assert.assertEquals("Soccer", marta.getSport().getName());

    }
}
