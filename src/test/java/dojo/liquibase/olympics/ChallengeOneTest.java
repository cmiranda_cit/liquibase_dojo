package dojo.liquibase.olympics;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import dojo.liquibase.AbstractLiquibaseBootstrapTest;
import dojo.liquibase.olympics.entity.Athlete;
import dojo.liquibase.olympics.entity.Country;
import dojo.liquibase.olympics.entity.Sport;

public class ChallengeOneTest extends AbstractLiquibaseBootstrapTest {

	@Before
	public void setup() {

		Country brasil = new Country();
		brasil.setName("Brasil");
		brasil.save();

		Sport judo = new Sport();
		judo.setName("Judo");
		judo.save();
	}

	@Test
	public void shouldInsertBrasilCountry() {

		Assert.assertEquals("Brasil", new Country().findByName("Brasil").getName());
	}

	@Test
	public void shouldInsertJudoSport() {

		Assert.assertEquals("Judo", new Sport().findByName("Judo").getName());
	}

	@Test
	public void shouldInsertRafaelaSilvaAthlete() {

		Athlete rafaelaSilva = new Athlete();
		rafaelaSilva.setName("Rafaela Silva");
		rafaelaSilva.setCountry(new Country().findByName("Brasil"));
		rafaelaSilva.setSport(new Sport().findByName("Judo"));
		Assert.assertTrue(rafaelaSilva.save());

		Assert.assertEquals("Rafaela Silva", new Athlete().findByName("Rafaela Silva").getName());

	}

}
