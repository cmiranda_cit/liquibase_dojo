package dojo.liquibase;

import org.junit.BeforeClass;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;

public abstract class AbstractLiquibaseBootstrapTest {

	public static void runChangeLog() {
		System.out.println("GO!");

		try {
			Database database = DatabaseFactory.getInstance()
					.findCorrectDatabaseImplementation(new JdbcConnection(Db.getConnection()));

			Liquibase liquibase = new Liquibase("db/sqlite/changelog.xml",
					new ClassLoaderResourceAccessor(), database);

			liquibase.update("");

		} catch (DatabaseException e) {
			e.printStackTrace();
		} catch (LiquibaseException e) {
			e.printStackTrace();
		}

	}

	@BeforeClass
	public static void beforeClassTest() {
		runChangeLog();
	}

}
